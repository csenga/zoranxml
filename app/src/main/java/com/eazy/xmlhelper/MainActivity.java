package com.eazy.xmlhelper;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.ResultReceiver;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;


public class MainActivity extends Activity {

	SharedPreferences prefs;
	
	ProgressDialog mProgressDialog;
	
	public static final String PREFS_NAME = "PREFERENCES.prefs";
	public static final String DOWNLOAD_URL="https://www.dropbox.com/s/kzxyb91qdv0qysc/examp.xml?dl=1";
	
	public static final String SAVED_FILE_NAME="temp.xml";
	XMLHandler handler;
	
	
	
	private class DownloadReceiver extends ResultReceiver{
	    public DownloadReceiver(Handler handler) {
	        super(handler);
	    }

	    @Override
	    protected void onReceiveResult(int resultCode, Bundle resultData) {
	        super.onReceiveResult(resultCode, resultData);
	        if (resultCode == DownloadService.UPDATE_PROGRESS) {
	            int progress = resultData.getInt("progress");
	            mProgressDialog.setProgress(progress);
	            if (progress == 100) {
	                mProgressDialog.dismiss();

                    if(!resultData.getBoolean("ERROR")) {
                        showList();
                    }
                    else
                    {
                        Toast.makeText(getApplicationContext(),"Nem aldoztal kecske!!!!",0).show();
                        clearList();
                    }
	            }
	        }
	    }
	}
	
	
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        
        prefs=getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        
      
         handler=new XMLHandler(this);
         
         
         mProgressDialog = new ProgressDialog(this);
         mProgressDialog.setMessage("Downloading...");
         mProgressDialog.setIndeterminate(true);
         mProgressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
         mProgressDialog.setCancelable(true);
         
         
         downloadXML();
       
    }
    
    public class CustomAdapter extends BaseAdapter
    {
    	Context cntx;
    	
    	
    	ArrayList<Item> items=new ArrayList<Item>();
    	
    	public CustomAdapter(Context cntx,ArrayList<Item> items) {
    		
    		super();
			this.cntx=cntx;
			
			this.items=items;
			
		}
    	
    	
    	
		@Override
		public int getCount() {
			
			return items.size();
		}

		@Override
		public Object getItem(int arg0) {
			
			return items.get(arg0);
		}

		@Override
		public long getItemId(int arg0) {
			
			return 0;
		}

		LayoutInflater inflanter;
		@Override
		public View getView(int pos, View view, ViewGroup group) {
			
			if(inflanter==null)
			{
				inflanter=LayoutInflater.from(cntx);
			}
			
			view =inflanter.inflate(R.layout.list_item, group, false);
			
			TextView name=(TextView) view.findViewById(R.id.name);
			
			TextView content=(TextView) view.findViewById(R.id.content);
			
			content.setText(items.get(pos).link);
			name.setText(items.get(pos).title);
			
			
			Log.i("IC","IC");
			return view;
		
		}
    	
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.retry) {
        	downloadXML();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
    
    ArrayList<Item> itemList;

    private void clearList()
    {
        ListView list=(ListView) findViewById(R.id.list);

        itemList=new ArrayList<Item>();
        CustomAdapter adapter=new CustomAdapter(this,itemList);

        list.setAdapter(adapter);

    }


    private void showList()
    {
	  ListView list=(ListView) findViewById(R.id.list);
      
	 itemList=null;
	  
	  try{
	 itemList= handler.parseXML();
	  }
	  catch(Exception e)
	  {
		  e.printStackTrace();
		  Toast.makeText(this, "Problem occured while reading the downloaded file!", Toast.LENGTH_LONG).show();
		  return;
	  }
	  
      CustomAdapter adapter=new CustomAdapter(this,itemList);
        
      list.setAdapter(adapter);
      list.setOnItemClickListener(new OnItemClickListener() {

		@Override
		public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
				long arg3) {
			
			String url=itemList.get(arg2).link;
			
					if (!url.startsWith("http://") && !url.startsWith("https://"))
						   url = "http://" + url;
			Uri uri = Uri.parse(url);
			try{
            startActivity(new Intent(Intent.ACTION_VIEW, uri));
			}
			catch(Exception e)
			{
				Toast.makeText(getApplicationContext(), "Problem occured while opening the link.", Toast.LENGTH_SHORT).show();
			}
		}
	});
    }
    
   
    
    private void downloadXML()
    {
    	String path=DOWNLOAD_URL;
		mProgressDialog.show();
    	Intent intent = new Intent(this, DownloadService.class);
    	intent.putExtra("url", DOWNLOAD_URL);
    	intent.putExtra("receiver", new DownloadReceiver(new Handler()));
    	startService(intent);
    	
    }
    
//    private void showURLDialog()
//    {
//    	LayoutInflater li = LayoutInflater.from(this);
//		View promptsView = li.inflate(R.layout.prompts, null);
//
//		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
//				this);
//
//		// set prompts.xml to alertdialog builder
//		alertDialogBuilder.setView(promptsView);
//
//		final EditText userInput = (EditText) promptsView
//				.findViewById(R.id.editTextDialogUserInput);
//		
//		userInput.setText(prefs.getString(DOWNLOAD_URL, ""));
//
//		// set dialog message
//		alertDialogBuilder
//			.setCancelable(false)
//			.setPositiveButton("OK",
//			  new DialogInterface.OnClickListener() {
//			    public void onClick(DialogInterface dialog,int id) {
//				
//			    	Editor editor = prefs.edit();
//			    	
//			    	editor.putString(DOWNLOAD_URL, userInput.getText().toString());
//			    	editor.commit();
//			//	result.setText();
//			    }
//			  })
//			.setNegativeButton("Cancel",
//			  new DialogInterface.OnClickListener() {
//			    public void onClick(DialogInterface dialog,int id) {
//				dialog.cancel();
//			    }
//			  });
//
//		// create alert dialog
//		AlertDialog alertDialog = alertDialogBuilder.create();
//
//		// show it
//		alertDialog.show();
//
//    }
}
