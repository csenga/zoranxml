package com.eazy.xmlhelper;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import android.app.DownloadManager;
import android.content.Context;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.OpenableColumns;
import android.util.Xml;

public class XMLHandler {

	
	Context cntx;
	
	
	public XMLHandler(Context cntx) {
		
		this.cntx=cntx;
	}
	
//	public void downloadXML(String path)
//	{
//		 File file = new File(cntx.getExternalFilesDir(null), MainActivity.SAVED_FILE_NAME);
//
//		 if(file.exists())
//		 {
//			 file.delete();
//		 }
//		 file.canRead();
//		 
//		String url = path;
//		DownloadManager.Request request = new DownloadManager.Request(Uri.parse(url));
//		request.setDescription("Downloading in progress");
//		
//		request.setTitle("XMLDownloading");
//	
//		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
//		    request.allowScanningByMediaScanner();
//		    request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
//		}
//		
//		
//		
//request.setDestinationUri(Uri.fromFile(file));
//		DownloadManager manager = (DownloadManager)cntx. getSystemService(Context.DOWNLOAD_SERVICE);
//		//;manager.enqueue(request)
//	manager.getUriForDownloadedFile(manager.enqueue(request));
//		
//	}
	public ArrayList<Item> parseXML() throws XmlPullParserException, IOException
	{
		ArrayList<Item> list = null;
		
		 XmlPullParser parser = Xml.newPullParser();
		 
		 
		FileInputStream fis = cntx.openFileInput( MainActivity.SAVED_FILE_NAME);
		 parser.setInput(fis, null);
		
		
	        int eventType = parser.getEventType();
	       Item currentItem=null;
	       
	        while (eventType != XmlPullParser.END_DOCUMENT){
	            String name = null;
	            switch (eventType){
	                case XmlPullParser.START_DOCUMENT:
	                	list=new ArrayList<Item>();
	                    break;
	                case XmlPullParser.START_TAG:
	                    name = parser.getName();
	                    if (name.equalsIgnoreCase("item")){
	                        currentItem=new Item();
	                    } else if (currentItem != null){
	                        if (name.equalsIgnoreCase("title")){
	                            currentItem.title = parser.nextText();
	                        } else if (name.equalsIgnoreCase( "link")){
	                        	currentItem.link = parser.nextText();
	                        } 
	                    }
	                    break;
	                case XmlPullParser.END_TAG:
	                    name = parser.getName();
	                    if (name.equalsIgnoreCase("item") && currentItem != null){
	                    	list.add(currentItem);
	                    } 
	            }
	            eventType = parser.next();
	        }
	        
	        return list;
	}

}
